public class DVD extends Item {
    public DVD(String title, String year, int available) {
        super(title, year, available);
        this.itemType = "DVD";
    }

}