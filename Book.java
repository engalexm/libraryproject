public class Book extends Item {
    private String author;

    public Book(String title, String author, String year, int available) {
        super(title, year, available);
        this.author = author;
        this.itemType = "Book";
    }

    public String getAuthor() {
        return this.author;
    }
}