public abstract class Item implements Borrowable {
    private String title;
    private String year;
    private int available;
    public String itemType;
    

    public Item(String title, String year, int available) {
        this.title = title;
        this.year = year;
        this.available = available;
    }

    public String getTitle() {
        return this.title;
    }

    public String getYear() {
        return this.year;
    }

    public int numAvailable() {
        return this.available;
    }

    public void decreaseNumAvailable() {
        this.available--;
    }

    public void increaseNumAvailable() {
        this.available++;
    }

    // Equality check: title, year, AND type must match. That means [Holes - 2005 - DVD] != [Holes - 2005 - Book]
    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Item)) return false;
        Item other = (Item) o;
        return other.getTitle().equals(this.getTitle()) && other.getYear().equals(this.getYear()) && this.itemType.equals(other.itemType);
    }

    public boolean checkOut() {
        if(this.numAvailable() == 0) {
            System.out.println("ERROR: Zero copies of " + this.getTitle() + " are available in the library!");
            return false;
        } else {
            this.decreaseNumAvailable();
            return true;
        }
    }

    public boolean returnItem() {
        this.increaseNumAvailable();
        return true;
    }

}
