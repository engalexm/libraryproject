public class CD extends Item {
    private String artist;

    public CD(String title, String artist, String year, int available) {
        super(title, year, available);
        this.artist = artist;
        this.itemType = "CD";
    }

    public String getArtist() {
        return this.artist;
    }

    
}