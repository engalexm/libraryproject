public class Periodical extends Item {
    private int volumeNum;
    private int issueNum;
    
    public Periodical(String title, String year, int volumeNum, int issueNum, int available) {
        super(title, year, available);
        this.volumeNum = volumeNum;
        this.issueNum = issueNum;
        this.itemType = "Periodical";
    }

    public int getVolumeNum() {
        return this.volumeNum;
    }

    public int getIssueNum() {
        return this.issueNum;
    }

}