import java.util.ArrayList;

public class Library {
    private ArrayList<Item> items = new ArrayList<Item>();

    public Library() {}
    
    public void addItem(Item i) {
        this.items.add(i);
        System.out.println("Item " + i.getTitle() + " added!");
    }

    public void removeItem(Item i) {
        this.items.remove(i);
        System.out.println("Item " + i.getTitle() + " removed!");
    }

    public boolean containsItem(Item i) {
        return this.items.contains(i);
    }

    public void checkOut(Item i) {
        if(!this.containsItem(i)) {
            System.out.println("ERROR: Item " + i.getTitle() + " does not exist.");
            return;
        }
        if(i.itemType.equals("Periodical")) {
            System.out.println("ERROR: Cannot take out periodicals.");
            return;
        }
        
        if(this.items.get(this.items.indexOf(i)).checkOut()) System.out.println("Successfully borrowed " + i.getTitle());
        return;
    }

    public void returnItem(Item i) {
        if(!this.containsItem(i)) {
            System.out.println("ERROR: Item " + i.getTitle() + " does not exist in library -- please use ADD function instead.");
            return;
        }
        if(i.itemType.equals("Periodical")) {
            System.out.println("ERROR: Cannot return periodicals.");
            return;
        }
        if(this.items.get(this.items.indexOf(i)).returnItem()) System.out.println("Successfully returned " + i.getTitle());
        return;
    }


    public void printLibrary() {
        System.out.println("======= YOUR LIBRARY =======");
        for(Item i : this.items) {
            System.out.println(i.getTitle() + "\t" + i.getYear() + "\t" + i.itemType + "\t" + i.numAvailable() + " available");
        }
        System.out.println("============================");
    }

}