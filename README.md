# Library Project
## Alex Eng - NIJ1
## Citi App Dev Training

Below is the library project prompt as copied from [Nick Todd's repo](https://github.com/nicktodd/java-course/):

    # Mini Development Project

    ## The Library Project

    Create a new project, and within the Project, create a set of classes that could be used to represent a traditional library.

    You will need to create a class to represent the library itself, and then a set of classes for items that could be made available from the library (eg. Books, CDs, DVDs and Periodicals).

    The Library class will allow users to have items added to it and items removed from it. It will also be possible to borrow items from the library.

    Periodicals cannot be borrowed. You need to ensure that this business rule is met.

    Do not create more than 6 items of each type in your library to demonstrate a working example.

    Create either a series of JUnit test classes or a simple main method (whichever you think is most appropriate) that will demonstrate the ability to borrow, add, and remove books and the various other features of your library.

    Pay particular attention to:

    1. Appropriate application of concepts such as encapsulation, inheritance and polymorphism
    2. Appropriate use of properties / methods
    3. Appropriate use of abstract classes and interfaces
    4. Suitable constructors if required
    5. Appropriate use of static properties / methods
    6. Readable and maintainable code
    7. A simple readme.md document specifying any design decisions and assumptions that you have made as you have implemented this project.

    You may get an opportunity on Friday to demonstrate this project to peers and managers.

# Design Decisions

I decided to use *Item* as an abstract class from which Book, CD, DVD, and Periodical inherit. All Items are given at least a title and year as well as a quantity of the item in stock. Certain items have other specific metadata fields, i.e. Book has "author," CD has "artist," etc.

*Item* also implements the *Borrowable* interface, which provides each item object with a simple ability to increment or decrement its quantity in stock during check-out or return transactions. Please note that this interface does nothing more than changing the counts. Actually maintaining object deletion & insertion is the responsibility of the *Library* class.

The *Library* class is responsible for maintaining an ArrayList of Items, which can be added or removed on the fly. This list can be checked for item containment as well as printed easily for convenience. Items can be looked up via the @Override of the `equals` method extant in the *Item* class. This equality check is based on Title + Year + Medium Type. The *Library* class is responsible for handling check-out and return requests via methods which take in concrete *Item* objects from the query, perform some business logic rules, and then call the check-out/return methods in the *Item* class for changing counts.

The *LibraryApp* class contains the `main` driver method used to test functionality. See that file for more information.