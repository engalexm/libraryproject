public interface Borrowable {

    public boolean checkOut();
    public boolean returnItem();

    

}