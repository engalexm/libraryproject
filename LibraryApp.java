public class LibraryApp {
    public static void main(String[] args) {

        // Testing core library functionalities
        Library myLibrary = new Library();

        // Add some books
        myLibrary.addItem(new Book("Lolita", "Vladimir Nabokov", "1955", 3));
        myLibrary.addItem(new Book("The Great Gatsby", "F. Scott Fitzgerald", "1925", 1));
        myLibrary.addItem(new Book("Ulysses", "James Joyce", "1922", 2));

        // Add some CDs
        myLibrary.addItem(new CD("My Beautiful Dark Twisted Fantasy", "Kanye West", "2010", 2));
        myLibrary.addItem(new CD("In the Court of the Crimson King", "King Crimson", "1969", 1));
        myLibrary.addItem(new CD("All Eyez On Me", "Tupac Shakur", "1996", 5));

        // Add some DVDs
        myLibrary.addItem(new DVD("Pulp Fiction", "1994", 2));
        myLibrary.addItem(new DVD("The End of Evangelion", "1997", 3));
        myLibrary.addItem(new DVD("Parasite", "2019", 1));

        // Add some Periodicals
        myLibrary.addItem(new Periodical("The New Yorker", "2020", 8, 2, 5));

        // Print library right now
        myLibrary.printLibrary();

        // Borrow items -- see qty decrease from 1 to 0
        Item toBorrow = new CD("In the Court of the Crimson King", "King Crimson", "1969", 1);
        myLibrary.checkOut(toBorrow);
        myLibrary.printLibrary();

        // Return items -- see qty increase from 0 to 1
        Item toReturn = new CD("In the Court of the Crimson King", "King Crimson", "1969", 1);
        myLibrary.returnItem(toReturn);
        myLibrary.printLibrary();

        // Borrow items illegally -- note qty can be 1 or anything; equality check uses title-year-type & not qty
        // Can't borrow periodicals
        Item illegalBorrow1 = new Periodical("The New Yorker", "2020", 8, 2, 1);
        myLibrary.checkOut(illegalBorrow1);
        // Can't borrow non-existent items
        Item illegalBorrow2 = new CD("To Pimp a Butterfly", "Kendrick Lamar", "2015", 1);
        myLibrary.checkOut(illegalBorrow2);
        // Can't borrow items with quantity 0 (see I borrow it twice from qty 1 -> 0 and again with qty 0, leading to error)
        Item illegalBorrow3 = new DVD("Parasite", "2019", 1);
        myLibrary.checkOut(illegalBorrow3);
        myLibrary.checkOut(illegalBorrow3);

        // Return items illegally -- note qty can be 1 or anything; equality check uses title-year-type & not qty
        // Can't return periodicals, as you can't borrow them to begin with
        Item illegalReturn1 = new Periodical("The New Yorker", "2020", 8, 2, 1);
        myLibrary.returnItem(illegalReturn1);
        // Can't return items that don't exist in library to begin with
        Item illegalReturn2 = new CD("To Pimp a Butterfly", "Kendrick Lamar", "2015", 1);
        myLibrary.returnItem(illegalReturn2);

        // Confirm nothing borrowed/returned in library
        myLibrary.printLibrary();
        
        // Add new items not listed in library
        Item newItem = new DVD("Snowpiercer", "2013", 5);
        myLibrary.addItem(newItem);
        myLibrary.printLibrary();

        // Remove existing items (all quantities) given they exist in library
        Item toDelete = new DVD("Snowpiercer", "2013", 1);
        myLibrary.removeItem(toDelete);
        myLibrary.printLibrary();


    }
}